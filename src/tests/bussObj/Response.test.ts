import {PostRespBusinessObjects} from "../../mapping/bussObj/Response";

describe("Generic Response generator test case", ()=>{
    it('Generate a generic JSON response message', ()=>{
        let genPostingResponse: PostRespBusinessObjects.PostingResponse = 
            new PostRespBusinessObjects.PostingResponse();
        let output: String = genPostingResponse.generateBasicResponse("0000", "Success!");
        expect(output).toEqual('{"metadata":{"status":"0000","description":"Success!"}}');
    });

    it('Generate response message with one trace', ()=>{
        let genPostingResponse: PostRespBusinessObjects.PostingResponse = 
            new PostRespBusinessObjects.PostingResponse();
        let output: String = genPostingResponse.generateBasicResponse("0000", "Success!",new PostRespBusinessObjects.Trace("CORE","Error occured in the core"));        
        expect(output).toEqual('{"metadata":{"status":"0000","description":"Success!","trace":[{"source":"CORE","description":"Error occured in the core"}]}}');
    });

    it('Generate response message with two traces', ()=>{
        let genPostingResponse: PostRespBusinessObjects.PostingResponse = 
            new PostRespBusinessObjects.PostingResponse();
        let output: String = genPostingResponse.generateBasicResponse("0000", "Success!",
                new PostRespBusinessObjects.Trace("CORE","Error occured in the core"),
                new PostRespBusinessObjects.Trace("CORE","Error occured in the core"));        
        expect(output).toEqual('{"metadata":{"status":"0000","description":"Success!","trace":[{"source":"CORE","description":"Error occured in the core"},{"source":"CORE","description":"Error occured in the core"}]}}');
    });    

    it('Check all the getters / setters', ()=>{
        let genPostingResponse: PostRespBusinessObjects.PostingResponse = 
            new PostRespBusinessObjects.PostingResponse();
        let output: String = genPostingResponse.generateBasicResponse("0000", "Success!",new PostRespBusinessObjects.Trace("CORE","Error occured in the core"));        
        expect(output.length>0).toBeTruthy();
        expect(genPostingResponse.metadataRoot.status).toEqual("0000");
    });

    it('Instantiate Trace object',() => {
        let traceBO: PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("INT","UT");
        expect(traceBO).toBeDefined();
        expect(traceBO.source).toBe("INT");
        expect(traceBO.description).toBe("UT");
    });

});
import DepositController from "../mapping/DepositController";

describe("Test cases for Depsoit Controller", () => {
    test("Instantiate the controller", () => {
        let depositController:DepositController = new DepositController("1234567");
        expect(depositController).toBeDefined();
    });

    test("Pass invalid CIF", () => {
        let depositController:DepositController = new DepositController("9934567");
        expect(depositController).toBeDefined();
        depositController.getDepsits().then((response:String) => {
            fail(response);
        }).catch((error:String) => {
            expect(JSON.parse(error.toString()).metadata.status).toBe("8404");
        });
    });

    test("Pass valid CIF", () => {
        let depositController:DepositController = new DepositController("1234567");
        expect(depositController).toBeDefined();
        depositController.getDepsits().then((response:String) => {
            let jsonObj:any = JSON.parse(response.toString());
            expect(jsonObj.metadata.status).toBe("0000");            
            expect(jsonObj.deposits.length > 0).toBeTruthy();
        }).catch((error:String) => {
            fail(error);
        });
    });
});
/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express from "express";
import {PostRespBusinessObjects}    from "../mapping/bussObj/Response";
import DepositController            from "../mapping/DepositController";

const router: any = express.Router();


router.get('/deposit/:id',(req: any, res: any) => {
    res.set("Content-Type","application/json; charset=utf-8");    
    if(req.params.id === undefined || req.params.id.length !== 7){
        res.status(404);   
        res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9404","Invalid Request!"));        
    }else{
        // Call Deposit Controller
        let depositController:DepositController = new DepositController(req.params.id);
        depositController.getDepsits().then((response:String) => {
            res.send(response);
        }).catch((error:any) => {
            res.send(error);
        });
    }    
});

/**
 * Routes Definition for health, readiness and liveness check
 */
 // Route for liveness prone
 // The kubelet kills the container and restarts it.
router.get('/healthz',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);    
    res.send();
});


// Route for rediness check. 
// This route will return 200 in-case all required bootstarap is finished
// Note: We want to suspend traffic in-case there is something wrong here
router.get('/readiness',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});


// Protect slow starting containers with startup probes
router.get('/startup',(_: any,res: any)=> {
    res.set("Content-Type","application/json; charset=utf-8");
    res.status(200);
    res.send();
});

export default router;
import depositJSON          from "../mapping/simulator/deposits.json"
import {PostRespBusinessObjects}        from "./bussObj/Response";
import DepositResponse,{DepositBO}        from "./bussObj/Deposits";

export default class DepsoitController{
    private _cif:string;

    constructor(cif: string){
        this._cif = cif;
    }

    /**
     * Get deposits from the simulator
     */
    getDepsits():Promise<String>{
        return new Promise<String>((resolve:any, reject:any)=>{
            let deposits: Array<DepositBO> = new Array<DepositBO>();
            let objCountr:number = 0;
            for(let index in depositJSON.deposits){
                if(depositJSON.deposits[index].cif === this._cif){
                    let depositBO:DepositBO = new DepositBO(                                              
                        depositJSON.deposits[index].contractID,               
                        depositJSON.deposits[index].type,                     
                        depositJSON.deposits[index].typeDesc,                 
                        depositJSON.deposits[index].name,                     
                        depositJSON.deposits[index].currency,                 
                        depositJSON.deposits[index].amount,                   
                        depositJSON.deposits[index].amountLCY,                
                        new Date(depositJSON.deposits[index].startDate),                
                        new Date(depositJSON.deposits[index].maturityDate),             
                        depositJSON.deposits[index].periodCode,               
                        depositJSON.deposits[index].autoRolloverFlag,         
                        (depositJSON.deposits[index].autoRolloverCounter as number),      
                        depositJSON.deposits[index].drawdownAccountNumber,    
                        depositJSON.deposits[index].princLiquidationAccount,  
                        depositJSON.deposits[index].profitLiquidationAccount, 
                        depositJSON.deposits[index].branchID,                 
                        (depositJSON.deposits[index].profitRate as number),               
                        depositJSON.deposits[index].category,                 
                        depositJSON.deposits[index].status,                   
                        depositJSON.deposits[index].accountTitle,             
                        depositJSON.deposits[index].rolloverType    
                    );
                    objCountr++;
                    deposits.push(depositBO);
                }
            }
            if(objCountr > 0){
                let metadata:PostRespBusinessObjects.Metadata = new PostRespBusinessObjects.Metadata();
                metadata.status = "0000";
                metadata.description = "Success!";
                metadata.responseTime = new Date()
                resolve(new DepositResponse(metadata,deposits).generateJSON());
            }else{
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8404","Record not found"));         
            }
        });
    }
}
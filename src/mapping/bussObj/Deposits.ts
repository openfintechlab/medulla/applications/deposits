import {PostRespBusinessObjects} from "./Response";

interface IDeposit {
    
    contractID:               string;
    type:                     string;
    typeDesc:                 string;
    name:                     string;
    currency:                 string;
    amount:                   number;
    amountLCY:                number;
    startDate:                Date;
    maturityDate:             Date;
    periodCode:               string;
    autoRolloverFlag:         string;
    autoRolloverCounter:      number;
    drawdownAccountNumber:    string;
    princLiquidationAccount:  string;
    profitLiquidationAccount: string;
    branchID:                 string;
    profitRate:               number;
    category:                 string;
    status:                   string;
    accountTitle:             string;
    rolloverType?:            string;
}

export class DepositBO implements IDeposit{    
    contractID:               string;
    type:                     string;
    typeDesc:                 string;
    name:                     string;
    currency:                 string;
    amount:                   number;
    amountLCY:                number;
    startDate:                Date;
    maturityDate:             Date;
    periodCode:               string;
    autoRolloverFlag:         string;
    autoRolloverCounter:      number;
    drawdownAccountNumber:    string;
    princLiquidationAccount:  string;
    profitLiquidationAccount: string;
    branchID:                 string;
    profitRate:               number;
    category:                 string;
    status:                   string;
    accountTitle:             string;
    rolloverType?:            string;
    
    constructor(
        
        contractID:               string,
        type:                     string,
        typeDesc:                 string,
        name:                     string,
        currency:                 string,
        amount:                   number,
        amountLCY:                number,
        startDate:                Date,
        maturityDate:             Date,
        periodCode:               string,
        autoRolloverFlag:         string,
        autoRolloverCounter:      number,
        drawdownAccountNumber:    string,
        princLiquidationAccount:  string,
        profitLiquidationAccount: string,
        branchID:                 string,
        profitRate:               number,
        category:                 string,
        status:                   string,
        accountTitle:             string,
        rolloverType?:            string,
    ){        
        this.contractID               =	contractID               
        this.type                     =	type                     
        this.typeDesc                 =	typeDesc                 
        this.name                     =	name                     
        this.currency                 =	currency                 
        this.amount                   =	amount                   
        this.amountLCY                =	amountLCY                
        this.startDate                =	startDate                
        this.maturityDate             =	maturityDate             
        this.periodCode               =	periodCode               
        this.autoRolloverFlag         =	autoRolloverFlag         
        this.autoRolloverCounter      =	autoRolloverCounter      
        this.drawdownAccountNumber    =	drawdownAccountNumber    
        this.princLiquidationAccount  =	princLiquidationAccount  
        this.profitLiquidationAccount =	profitLiquidationAccount 
        this.branchID                 =	branchID                 
        this.profitRate               =	profitRate               
        this.category                 =	category                 
        this.status                   =	status                   
        this.accountTitle             =	accountTitle             
        this.rolloverType            =	rolloverType            

    }
}

export default class DepositResponse{
    private metadata!: PostRespBusinessObjects.Metadata;
    private deposits!:DepositBO[];

    constructor(metadata: PostRespBusinessObjects.Metadata, deposits:DepositBO[]){
        this.metadata = metadata;
        this.deposits = deposits;
    }

    public generateJSON(): String{
        return JSON.stringify(this);
    }
}